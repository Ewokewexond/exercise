module Main where
  import Data.List
  import Data.Array

  -- morse code exercise from
  -- https://old.reddit.com/r/dailyprogrammer/comments/cmd1hb/20190805_challenge_380_easy_smooshed_morse_code_1/
  --
  --
  -- implement smorse, which converts a string into morse code without spaces (smooshed morse)
  -- tbd

  morse = ".- -... -.-. -.. . ..-. --. .... .. .--- -.- .-.. -- -. --- .--. --.- .-. ... - ..- ...- .-- -..- -.-- --.."
  letters = ['a'..'z']
  dict = zip letters $ words morse

  dictMorse = listArray ('a','z') $ words morse
 
  smorse :: [Char] -> [Char]
  smorse c = foldr (++) [] $ map (\x -> dictMorse ! x) $ filter (/= ' ') c 

  smalpha :: [Char] -> [Char]
  smalpha a = map (\y -> if y /= '.' then 't' else 'e') a

  main =
    putStrLn "Void"
